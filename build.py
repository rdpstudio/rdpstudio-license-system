import random
import string
import os

class File:

    chunk_size = (64 << 10) - 1

    @classmethod
    def read(cls, file, _bytes=True, chunk_size=None):

        if not chunk_size:
            chunk_size = cls.chunk_size

        with open(file, 'rb' if _bytes else 'rt', encoding="utf-8") as f:
            while True:
                data = f.read(chunk_size)
                if data:
                    yield data
                else:
                    break

    @classmethod
    def write(cls, file, data):
        with open(file, 'wb') as f:
            for n in range(0, len(data), cls.chunk_size):
                _max = n + cls.chunk_size
                _data = data[n:_max]
                f.write(_data.encode() if isinstance(_data, str) else _data)

appid = ''.join(random.sample(string.ascii_letters + string.digits, 50))
print("Generated APPID:" + appid)
appkey = ''.join(random.sample(string.ascii_letters + string.digits, 50))
print("Generated APPKEY:" + appkey)
aeskey = ''.join(random.sample(string.digits + string.digits + string.digits + string.digits + string.digits, 32))
print("Generated AESKEY:" + aeskey)
f = os.popen('git describe --tags --always --dirty="-dev" --long', "r")
rlsver = f.read()
f.close()
rlsver = rlsver.replace("\n", "")
print("Program Version:" + rlsver)

def write_template(template, py_temp, _dict):
    data = ''
    for _data in File.read(template, False):
        data += _data
    File.write(py_temp, replace(data, _dict))

def replace(data, _dict):
    for k in _dict:
        data = data.replace(k, _dict[k])
    return data

_dict = {
            'appid': str(appid),
            'appkey': str(appkey),
        }

write_template(os.path.join('changeing_code', 'license_easy.py'), os.path.join('engine_code', 'license_easy.py'), _dict)
write_template(os.path.join('changeing_code', 'license_easy_private.py'), os.path.join('engine_code', 'license_easy_private.py'), _dict)

_dict = {
            'aeskey': str(aeskey),
            'rlsver': str(rlsver),
        }

write_template(os.path.join('changeing_code', 'license_engine.py'), os.path.join('engine_code', 'license_engine.py'), _dict)
write_template(os.path.join('changeing_code', 'license_engine_private.py'), os.path.join('engine_code', 'license_engine_private.py'), _dict)
