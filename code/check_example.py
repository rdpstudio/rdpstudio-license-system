# 如要使用pyinstaller等工具打包需要完整导入(引擎二次封装、引擎、引擎所用各种库) #
# Public版用于校验License、获取设备MAC Addr（不包含生成方法） #
import license_easy
import license_engine
import os
import uuid
import hashlib
import datetime
import base64
from Crypto.Cipher import AES
import ntplib
import time

print("Your Mac Addr: " + license_easy.get_mac())

lic = input("Enter your license key:")

if license_easy.check(lic,"example"):
    print("License is OK!")
else:
    print("License is not OK!")

os.system("pause")