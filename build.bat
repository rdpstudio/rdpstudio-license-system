@echo off
title RDPStudio License System Builder
call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\Tools\VsDevCmd.bat"
cd engine_code
del /f *.py
cd ..
py build.py
cd dist
easycython ..\engine_code\*.py
pyinstaller -F ..\code\gen_license.py
pyinstaller -F ..\code\check_example.py
copy dist\*.exe %cd%
rd /s /q build
rd /s /q dist
del /f *.spec
cd ..
cd engine_code
del /f %cd%\..\inbuild\c\*.c
del /f %cd%\..\inbuild\html\*.html
copy *.c %cd%\..\inbuild\c
copy *.html %cd%\..\inbuild\html
del /f *.c
del /f *.html
cd ..
cd code
rd /s /q __pycache__
cd ..
cd dist
copy license_easy.cp37-win_amd64.pyd %cd%\pyd\public
copy license_engine.cp37-win_amd64.pyd %cd%\pyd\public
copy license_easy_private.cp37-win_amd64.pyd %cd%\pyd\private
copy license_engine_private.cp37-win_amd64.pyd %cd%\pyd\private
del /f %cd%\*.pyd
cd ..
copy %cd%\code\*.py %cd%\dist
copy requirements.txt %cd%\dist\pyd
del /f dist.7z
"%ProgramFiles%\7-Zip\7z.exe" a -t7z dist.7z %cd%\dist
