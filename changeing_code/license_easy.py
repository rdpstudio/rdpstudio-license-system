from license_engine import *
import uuid

oper = LicenseEngine()

def get_mac():
    mac = oper.get_mac_address()
    return mac

def check(license, use):
    license_dic = oper.read_license(license)
    if license_dic:
        date_bool = oper.check_license_date(license_dic['time_str'])
        psw_bool = oper.check_license_psw(license_dic['psw'],use,"appid","appkey")
        if psw_bool:
            if date_bool:
                return True
            else:
                return False
        else:
            return False
    else:
        return False